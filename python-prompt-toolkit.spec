%global _empty_manifest_terminate_build 0
Name:		python-prompt-toolkit
Version:	3.0.48
Release:	1
Summary:	Library for building powerful interactive command lines in Python
License:	BSD-3-Clause
URL:		https://github.com/prompt-toolkit/python-prompt-toolkit
Source0:	https://files.pythonhosted.org/packages/source/p/prompt_toolkit/prompt_toolkit-%{version}.tar.gz
BuildArch:	noarch

%description
prompt_toolkit is a library for building powerful interactive command lines and
terminal applications in Python.

%package -n python3-prompt-toolkit
Summary:	Library for building powerful interactive command lines in Python
Provides:	python-prompt-toolkit = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-wcwidth
%description -n python3-prompt-toolkit
prompt_toolkit is a library for building powerful interactive command lines and
terminal applications in Python.

%package help
Summary:	Development documents and examples for prompt-toolkit
Provides:	python3-prompt-toolkit-doc
%description help
prompt_toolkit is a library for building powerful interactive command lines and
terminal applications in Python.

%prep
%autosetup -n prompt_toolkit-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-prompt-toolkit -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Nov 07 2024 panzhe <panzhe@kylinos.cn> - 3.0.48-1
- Update package to version 3.0.48
  * Add `@overload` to `contrib.regular_languages.compiler.Variables.get`.
  * Use `Sequence` instead of `list` for `words` argument in completers.
  * Display an "underscore" cursor in Vi's "replace single" mode, like "replace" mode.
  * Display an "beam" cursor in Emacs (insert) mode.

* Wed Aug 28 2024 xu_ping <707078654@qq.com> - 3.0.47-1
- Update package to version 3.0.47
  Accept os.PathLike in FileHistory
  Allow passing exception classes for KeyboardInterrupt and EOFError in PromptSession.

* Fri Mar 1 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 3.0.43-1
- Update package to version 3.0.43
  Make formatted_text.split_lines() accept an iterable instead of lists only.
  Disable the IPython workaround (from 3.0.41) for IPython >= 8.18.
  Restore signal.SIGINT handler between prompts.

* Tue Aug 01 2023 chenzixuan <chenzixuan@kylinos.cn> - 3.0.39-1
- Update package to version 3.0.39

* Fri Mar 17 2023 wubijie <wubijie@kylinos.cn> - 3.0.38-1
- Update package to version 3.0.38

* Tue Feb 28 2023 wangjunqi <wangjunqi@kylinos.cn> - 3.0.37-1
- Update package to version 3.0.37

* Fri Dec 16 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 3.0.36-1
- Update package to version 3.0.36

* Wed Nov 16 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.0.32-1
- Update package to version 3.0.32

* Fri Oct 28 2022 liqiuyu <liqiuyu@kylinos.cn> - 3.0.31-1
- Upgrade package to version 3.0.31

* Wed May 11 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 3.0.20-2
- License compliance rectification

* Sun Oct 17 2021 Li Chao <clouds@isrc.iscas.ac.cn> - 3.0.20-1
- First packaging of version 3.0.20
